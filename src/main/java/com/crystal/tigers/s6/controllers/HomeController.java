package com.crystal.tigers.s6.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Home controller
 */

@RestController
public class HomeController {

    @RequestMapping(method = RequestMethod.GET, path = "/")
    public String index() {
        return "Hello, Vinh!";
    }
}
